class BreedsController < ApplicationController

  def index

    @breed_type = 'akita'
    if params['breed']
      @breed_type = params['breed']
    end
    @breed = DogBreedFetcher.fetch(@breed_type)
    begin
      @breed_dog_url = JSON.parse(RestClient.get("https://dog.ceo/api/breed/#{@breed_type.gsub('-', '/')}/images/random").body)['message']
    rescue Object => e
    end
  end

end

