module BreedsHelper
  def breed_dropdown_list
    begin
      breeds_dropdown_list = JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)["message"]
      breeds = {}
      breeds_dropdown_list.keys.each do |f|
      	breeds[f.capitalize] = f
      	breeds_dropdown_list[f].each do |types|
      		breed = [f, types].join('-')
      		breeds[breed.titleize] = breed
      	end
      end
      breeds
    rescue Object => e
      []
    end
  end
end
